const AWS = require('aws-sdk');
const fs = require('fs');
const outputSuccess = './successfulDuplications.json';
const outputFailures = './failedDuplications.json';

let totalS3Data = [];

const successes = [];
const failures = [];

const {
  AWS_REGION,
  S3_SECRET_KEY,
  SOURCE_S3_BUCKET,
  DESTINATION_S3_BUCKET,
  S3_ACCESS_KEY
} = require('./keys');

const s3 = new AWS.S3({
  accessKeyId: S3_ACCESS_KEY,
  secretAccessKey: S3_SECRET_KEY,
  region: AWS_REGION
});

const listAllKeys = async token => {
  const opts = { Bucket: SOURCE_S3_BUCKET };
  if (token) {
    opts.ContinuationToken = token;
  }

  const response = await s3.listObjectsV2(opts).promise();

  totalS3Data = [...totalS3Data, ...response.Contents];
  console.log(
    `Total S3 Data: ${totalS3Data.length} objects from ${SOURCE_S3_BUCKET}`
  );

  console.log(`S3 response is IsTruncated: ${response.IsTruncated}`);

  if (response.IsTruncated) {
    await listAllKeys(response.NextContinuationToken);
  }
};

const duplicateS3Objects = async () => {
  let counter = totalS3Data.length;
  for (const data of totalS3Data) {
    const { Key } = data;

    var params = {
      Bucket: DESTINATION_S3_BUCKET,
      CopySource: `/${SOURCE_S3_BUCKET}/${Key}`,
      Key: Key
    };

    try {
      // Duplicate the original files to the new S3 bucket.
      await s3
        .copyObject(params)
        .promise()
        .then(results => {
          successes.push({ sourceObject: data, duplicationResults: results });
          console.log(`${counter} S3 Objects remaining.`);
          counter--;
        })
        .catch(error => {
          console.log(error);
          failures.push({ sourceObject: data, error: error });
        });
    } catch (error) {
      console.log(error);
      failures.push({ sourceObject: data, error: error });
    }
  }

  write(outputSuccess, successes);
  write(outputFailures, failures);
};

const write = (filePath, data) => {
  // Write file.
  fs.writeFile(filePath, JSON.stringify(data), err => {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log(`Successfully wrote to ${filePath}`);
  });
};

const summary = () => {
  console.log('====== SUMMARY ======');
  console.log(totalS3Data);
  console.log(
    `\nFound ${totalS3Data.length} S3 Objects to copy from ${SOURCE_S3_BUCKET} to ${DESTINATION_S3_BUCKET}.`
  );
  console.log('\n====== DUPLICATE OBJECTS ======');
  console.log(
    `You successfully duplicated ${successes.length} S3 Objects (${outputSuccess}).`
  );
  console.log(
    `You failed to duplicate ${failures.length} S3 Objects (${outputFailures}).`
  );
};

const main = async () => {
  await listAllKeys();
  await duplicateS3Objects();
  summary();
};

main();
