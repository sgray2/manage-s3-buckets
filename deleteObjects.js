const AWS = require('aws-sdk');
const fs = require('fs');
const inputData = './matchedData.json';
const outputSuccess = './successfulDeletions.json';
const outputFailures = './failedDeletions.json';

const successes = [];
const failures = [];

const {
  AWS_REGION,
  S3_SECRET_KEY,
  S3_BUCKET,
  S3_ACCESS_KEY
} = require('./keys');

const s3 = new AWS.S3({
  accessKeyId: S3_ACCESS_KEY,
  secretAccessKey: S3_SECRET_KEY,
  region: AWS_REGION
});

const rawData = fs.readFileSync(inputData);
const oldLinks = [...JSON.parse(rawData)].map(bucket => {
  return {
    fullPath: `/${S3_BUCKET}/${bucket.oldLink}`,
    bucketKey: bucket.oldLink,
    bucket: S3_BUCKET
  };
});

const deleteS3Objects = async () => {
  let counter = oldLinks.length;
  for (const link of oldLinks) {
    const { bucket, bucketKey } = link;
    var params = {
      Bucket: bucket,
      Key: bucketKey
    };

    try {
      // Delete the original files to the new S3 bucket.
      await s3
        .deleteObject(params)
        .promise()
        .then(results => {
          successes.push({ s3Object: link, deleteResults: results });
          console.log(`${counter} S3 Objects remaining.`);
          counter--;
        })
        .catch(error => {
          console.log(error);
          failures.push({ s3Object: link, error: error });
        });
    } catch (error) {
      console.log(error);
      failures.push({ s3Object: link, error: error });
    }
  }

  write(outputSuccess, successes);
  write(outputFailures, failures);
};

const write = (filePath, data) => {
  // Write file.
  fs.writeFile(filePath, JSON.stringify(data), err => {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log(`Successfully wrote to ${filePath}`);
  });
};

const summary = () => {
  console.log('====== SUMMARY ======');
  console.log(oldLinks);
  console.log(
    `\nFound ${oldLinks.length} S3 Objects to delete from ${S3_BUCKET}.`
  );
  console.log('\n====== DELETE OBJECTS ======');
  console.log(
    `You successfully deleted ${successes.length} S3 Objects (${outputSuccess}).`
  );
  console.log(
    `You failed to delete ${failures.length} S3 Objects (${outputFailures}).`
  );
};

const main = async () => {
  // await deleteS3Objects();
  summary();
};

main();
