const AWS = require('aws-sdk');
const path = require('path');
const util = require('util');
const csv = require('csv-parser');
const fs = require('fs');
const csvFile = './query.csv';

const ehsData = [];
const allBuckets = [];
const matchedData = [];
const missingData = [];
const successes = [];
const failures = [];
const brokenLinks = [];

const {
  AWS_REGION,
  S3_SECRET_KEY,
  S3_BUCKET,
  S3_ACCESS_KEY
} = require('./keys');

const s3 = new AWS.S3({
  accessKeyId: S3_ACCESS_KEY,
  secretAccessKey: S3_SECRET_KEY,
  region: AWS_REGION
});

fs.createReadStream(csvFile)
  .pipe(csv())
  .on('data', data => ehsData.push(data))
  .on('end', () => {
    console.log(`Finished parsing ${csvFile}`);
  });

const print = message => {
  console.log(
    util.inspect(message, {
      depth: null,
      maxArrayLength: null
    })
  );
};

let totalS3Data = [];
const listAllKeys = async token => {
  const opts = { Bucket: S3_BUCKET };
  if (token) {
    opts.ContinuationToken = token;
  }

  const response = await s3.listObjectsV2(opts).promise();

  const onlyOrganizationsWithNamesForFolders = response.Contents.filter(
    bucket => {
      const paths = bucket.Key.split(path.sep);
      const organizationName = paths[1];

      // Skip files that we have already moved (folders that are numbers)
      const thisFolderHasNotBeenCopiedBefore = isNaN(organizationName);

      return thisFolderHasNotBeenCopiedBefore;
    }
  );

  totalS3Data = [...totalS3Data, ...onlyOrganizationsWithNamesForFolders];
  console.log(`Total S3 Data length: ${totalS3Data.length}`);

  console.log(`S3 response is IsTruncated: ${response.IsTruncated}`);

  if (response.IsTruncated) {
    await listAllKeys(response.NextContinuationToken);
  }

  return totalS3Data;
};

const combineData = async () => {
  try {
    // Get all content in the S3_BUCKET.
    const response = await listAllKeys();

    // Get the bucket information.
    allBuckets.push(...response);

    for (const bucket of allBuckets) {
      const { Key } = bucket;

      // Each file in Ehs is linked to an S3 bucket Key.
      const baseName = path.basename(Key);

      // Match the ehsData with the s3 Key.
      const correspondingEhsData = ehsData.find(
        file => file.file_key === baseName
      );

      if (correspondingEhsData) {
        matchedData.push({
          ehsData: { ...correspondingEhsData },
          s3Data: { ...bucket }
        });
      } else {
        missingData.push({ s3Data: { ...bucket } });
      }
    }
  } catch (error) {
    console.log(error);
  }
};

const processData = async () => {
  for (const data of matchedData) {
    const { ehsData, s3Data } = data;

    // Separate the Key (file path) string for the Bucket.
    const paths = s3Data.Key.split(path.sep);

    // Get the original org name from the
    const oldOrgName = paths[paths.length - 3];
    const newOrgName = ehsData.org_name;
    const orgId = ehsData.org_id;

    // Store the old link.
    data.oldLink = s3Data.Key;
    // Create the new link.
    // Group / Organization id / Practice id / S3 Key
    data.newLink = path.join(
      `${paths[0]}`,
      `${ehsData.org_id}`,
      `${ehsData.practice_id}`,
      `${paths[3]}` // KEY
    );

    if (oldOrgName !== newOrgName) {
      brokenLinks.push(data);
      console.log(
        `File: ${ehsData.file_id} from organization ${orgId} was changed from:\n${oldOrgName} (S3)\nto\n${newOrgName} (EHS database)\n\n`
      );
    }
  }

  // console.log(
  //   'The following S3 Keys did not match any file keys in the EHS database.'
  // );
  // print({ missingData });
};

const moveData = async () => {
  let counter = matchedData.length;
  for (const data of matchedData) {
    const { oldLink, newLink } = data;

    var params = {
      Bucket: S3_BUCKET,
      CopySource: `/${S3_BUCKET}/${oldLink}`,
      Key: newLink
    };

    try {
      // Move the original files to the new S3 bucket.
      await s3
        .copyObject(params)
        .promise()
        .then(results => {
          successes.push({ originalData: data, copyResults: results });
          console.log(`${counter} S3 Buckets remaining.`);
          counter--;
        })
        .catch(error => {
          console.log(error);
          failures.push({ originalData: data, error: error });
        });
    } catch (error) {
      console.log(error);
      failures.push({ originalData: data, error: error });
    }
  }

  write('successfulCopies.json', successes);
  write('failures.json', failures);
};

const write = (filePath, data) => {
  // Write file.
  fs.writeFile(filePath, JSON.stringify(data), err => {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log(`Successfully wrote to ${filePath}`);
  });
};

const summary = () => {
  console.log('\n\n====== SUMMARY ======');
  console.log(`Found ${ehsData.length} records from EHS (from ${csvFile}).`);
  console.log(
    `Found ${allBuckets.length} S3 Buckets with the Organization Name as the folder name (from ${S3_BUCKET}).`
  );
  console.log(
    `Combined ${matchedData.length} EHS records with their S3 Buckets.`
  );
  console.log(
    `You have a total of ${brokenLinks.length} broken links out of the ${matchedData.length} files in EHS.`
  );
  console.log(
    `You have a total of ${missingData.length} S3 Keys with no corresponding EHS file key.`
  );
  console.log('\n====== COPY OBJECTS ======');
  console.log(
    `You successfully copied ${successes.length} S3 Buckets (successfulCopies.json).`
  );
  console.log(
    `You failed to copy ${failures.length} S3 Buckets (failures.json).`
  );
};

const main = async () => {
  await combineData();
  await processData();

  // Write files.
  write('matchedData.json', matchedData);
  write('missingData.json', missingData);

  // Uncomment to copy data.
  // await moveData();
  summary();
};

main();
